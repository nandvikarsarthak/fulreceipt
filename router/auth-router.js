const express = require('express');
const router = express.Router();
const authControllername = require('../controllers/auth-controller');

router.route('/').get(authControllername.home);
router.route('/register').post(authControllername.register);

module.exports = router;
