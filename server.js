require('dotenv').config();
const cors = require('cors');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json());
const fast2sms = require('fast-two-sms');
const router = require('./router/auth-router');
const connectDB = require('./utils/db');
const axios = require('axios');

const corsOptions = {
  origin: [
    // 'http://localhost:3000',
    'https://664b96c37c57ca032bac948d--tubular-kheer-664eae.netlify.app/',
    // 'https://www.fast2sms.com/dev/bulkV2',
    // 'http://localhost:5000/api/auth/register',
  ],
  methods: 'GET, POST, DELETE, PUT, PATCH, HEAD',
  credentials: 'true',
};
app.use(cors(corsOptions));

// app.post('/sendMessage', async (req, res) => {
//   const response = await fast2sms.sendMessage({
//     authorization: process.env.SMS_API_KEY,
//     message: req.body.particulars,
//     numbers: [req.body.phone],
//   });
// });
// ('https://www.fast2sms.com/dev/bulkV2', smsData, {
//   headers: {
//     Authorization: apiKey,
//   },
// })
// .then((response) => {
//   console.log('SMS sent Successfully:', response.data);
// })
// .catch((error) => {
//   console.log('Error SMS: ', error);
// });

app.use(express.json());
app.use('/', router);
app.use('/register', router);
// app.use('/api', router);
// app.use('/api/auth', router);
// app.use('/api/auth/register', router);

const PORT = process.env.PORT || 5000;

connectDB().then(() => {
  app.listen(PORT, () => {
    console.log(`Running on port ${PORT}`);
  });
});
