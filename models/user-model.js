const mongoose = require('mongoose');

const userSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      require: true,
    },
    phone: {
      type: String,
      require: true,
    },
    address: {
      type: String,
      require: true,
    },
    particulars: {
      type: String,
      require: true,
    },
    quantity1: {
      type: String,
      require: true,
    },
    quantity2: {
      type: String,
    },
    amount1: {
      type: String,
      require: true,
    },
    amount2: {
      type: String,
    },
    advance: {
      type: String,
    },
    balance: {
      type: String,
    },
    delivery: {
      type: String,
    },
    total: {
      type: String,
      require: true,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

//define the model or collection name
const User = new mongoose.model('User', userSchema);

module.exports = { User };
