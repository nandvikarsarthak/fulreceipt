const { User } = require('../models/user-model');
// const Receipt = require('../models/user-model');

const home = async (req, res) => {
  try {
    res.send(`The server is running using routers`);
  } catch (error) {
    console.log('Error');
  }
};

const register = async (req, res) => {
  try {
    console.log(req.body);
    const {
      name,
      phone,
      address,
      particulars,
      quantity1,
      quantity2,
      amount1,
      amount2,
      advance,
      balance,
      delivery,
      total,
    } = req.body;

    await User.create({
      name,
      phone,
      address,
      particulars,
      quantity1,
      quantity2,
      amount1,
      amount2,
      advance,
      balance,
      delivery,
      total,
    });
    res.send(body);
    res.json(data.body);
  } catch (error) {
    res.json('Internal Server Error From Controller');
  }
};

module.exports = { home, register };
