const axios = require('axios');
const cors = require('cors');
const dotenv = require('dotenv');
// const app = express();
// const User = require('../models/user-model');
const register = require('./controllers/auth-controller');
const express = require('express');
const { response } = require('express');
// const { User, Receipt } = require('./models/user-model');
dotenv.config();

const port = process.env.PORT;
const apiKey = process.env.SMS_API_KEY;
const message = 'Hi Hello ';
const phoneNumber = '7039967009';

const smsData = {
  message: message,
  language: 'english',
  route: 'q',
  numbers: phoneNumber,
};

axios

  .post('https://www.fast2sms.com/dev/bulkV2', smsData, {
    headers: {
      Authorization: apiKey,
    },
  })
  .then((response) => {
    console.log('SMS sent Successfully:', response.data);
  })
  .catch((error) => {
    console.log('Error SMS: ', error);
  });
